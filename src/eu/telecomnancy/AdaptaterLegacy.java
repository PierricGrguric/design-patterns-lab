package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class AdaptaterLegacy implements ISensor{

	private LegacyTemperatureSensor sensor;
	
	public AdaptaterLegacy(){
		sensor=new LegacyTemperatureSensor();
	}
	@Override
	public void on() {
		if (sensor.getStatus()==false){
			sensor.onOff();
		}
		
	}

	@Override
	public void off() {
		if (sensor.getStatus()==true){
			sensor.onOff();
		}
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (sensor.getStatus())
			sensor.getTemperature();
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");

		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getTemperature();
	}
	public double getUnit(){
		return 0;
	}
	 public void changeUnit(){};
}
