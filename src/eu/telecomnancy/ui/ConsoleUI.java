package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConsoleUI {

    private ISensor sensor;
    private Scanner console;

    public ConsoleUI(ISensor sensor) {
        this.sensor = sensor;
        this.console = new Scanner(System.in);
        manageCLI();
    }

    public void manageCLI() {
        String rep = "";
        System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value - unit|n: change unit");
        while (!"q".equals(rep)) {
            try {
                System.out.print(":> ");
                rep = this.console.nextLine();
                if ("on".equals(rep) || "o".equals(rep)) {
                    this.sensor.on();
                    System.out.println("sensor turned on.");
                } else if ("off".equals(rep) || "O".equals(rep)) {
                    this.sensor.off();
                    System.out.println("sensor turned off.");
                } else if ("status".equals(rep) || "s".equals(rep)) {
                    System.out.println("status: " + this.sensor.getStatus());
                } else if ("update".equals(rep) || "u".equals(rep)) {
                    this.sensor.update();
                    System.out.println("sensor value refreshed.");
                }else if ("unit".equals(rep) || "n".equals(rep)) {
                    this.sensor.changeUnit();
                    System.out.println("unit changed");
                } else if ("value".equals(rep) || "v".equals(rep)) {
                	if(this.sensor.getUnit()==0){
                		System.out.println("value: " + this.sensor.getValue()+"�C");
                	}else if(this.sensor.getUnit()==1){
                		System.out.println("value: " + Math.floor(this.sensor.getValue())+"�C");
                	}else if(this.sensor.getUnit()==2){
                		System.out.println("value: " + (1.8*(this.sensor.getValue())+32)+"�F");
                	}
                } else {
                    System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value");
                }
            } catch (SensorNotActivatedException ex) {
                Logger.getLogger(ConsoleUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
